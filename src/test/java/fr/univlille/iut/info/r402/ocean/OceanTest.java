package fr.univlille.iut.info.r402.ocean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class OceanTest {
	


@Test
public void testPoissonDeplaceRandom() {
	Ocean ocean = new Ocean(10);
	Poisson poisson = new Poisson(ocean);
	int x = poisson.getX();
	int y = poisson.getY();

	ocean.nextDay();
	assertTrue(poisson.getX()!= x || poisson.getY()!=y);
	
}




}
