package fr.univlille.iut.info.r402.ocean;

public class Ocean {
	protected String[][] ocean;
	
	Ocean(int taille){
		this.ocean=new String[taille][taille];
	}

	public void nextDay() {
		// TODO Auto-generated method stub
		for (int i = 0 ; i<this.ocean.length;i++) {
			for (int y = 0 ; y<this.ocean[i].length;y++) {
				if (this.ocean[i][y].equals("🐟")) {
					int cote = (int) (Math.random()*4);
					if (cote==0) {
						if (y==this.ocean[i].length-1) {
							this.ocean[i][y]=null;
							this.ocean[i][0]="🐟";
						}
						else {
							this.ocean[i][y]=null;
							this.ocean[i][y+1]="🐟";
						}
					}
					else if (cote==1) {
						if (y==0) {
							this.ocean[i][y]=null;
							this.ocean[i][this.ocean[i].length-1]="🐟";
						}
						else {
							this.ocean[i][y]=null;
							this.ocean[i][y-1]="🐟";
						}
					}
					else if (cote==2) {
						if (i==this.ocean.length-1) {
							this.ocean[i][y]=null;
							this.ocean[0][y]="🐟";
						}
						else {
							this.ocean[i][y]=null;
							this.ocean[i+1][y]="🐟";
						}
					}
					else if (cote==3) {
						if (i==0) {
							this.ocean[i][y]=null;
							this.ocean[this.ocean.length-1][y]="🐟";
						}
						else {
							this.ocean[i][y]=null;
							this.ocean[i-1][y]="🐟";
						}
					}
				}
			}
		}
	}
	
	
}
